package it.unibo.oop.lab.mvc;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This class implements the interface "Controller".
 *
 */
public class ControllerImpl implements Controller {
    private final ArrayList<String> list = new ArrayList<String>();
    private int index;
    /**
     * Constructor that initialize the index at 0.
     */
    public ControllerImpl() {
        this.index = 0;
    }
    /**
     * add a string to the list (the next that will be printed).
     * @param str
     *          is the string
     * @throws IllegalArgumentException
     *          if the string is null
     */
    public void setNextString(final String str) throws IllegalArgumentException {
        if (str == null) {
            throw new IllegalArgumentException();
        }
        list.set(index, str);
    }
    /**
     * @return
     *          the next string to be printed
     */
    public String getNextString() {
        return list.get(index);
    }

    /**
     * this method creates a collection of already printed strings.
     * @return 
     *          all the strings already printed
     */
    public Collection<String> getAllPrintedStrings() {
        Collection<String> allPrintedStrings = new ArrayList<String>();
        int i = 0;
        for (; i < index; i++) {
            allPrintedStrings.add(list.get(i));
        }
        return allPrintedStrings;
    }
/**
 * this method prints the actual string on the standard output.
 * @throws IllegalStateException
 *      it the string is not unset
 */
    public void print() throws IllegalStateException {
       if (list.get(index) == null) {
           throw new IllegalStateException();
       }
       System.out.println(list.get(index));
       index = index + 1;
    }

}
