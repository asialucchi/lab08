package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import it.unibo.oop.lab.mvcio.Controller;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {

    /*
     * Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */

    private final JFrame frame = new JFrame();
    private Controller fileController = new Controller();

    /**
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUIWithFileChooser() {
        final JPanel panel = new JPanel();
        frame.setName("My first java graphical interface");
        frame.setContentPane(panel);
        panel.setLayout(new BorderLayout());
        final JTextArea textArea = new JTextArea();
        panel.add(textArea);
        final JButton save = new JButton("Save");
        panel.add(save, BorderLayout.SOUTH);
        final JPanel browsePanel = new JPanel();
        browsePanel.setLayout(new BorderLayout());
        panel.add(browsePanel, BorderLayout.NORTH);
        final JTextField blockedTextField = new JTextField();
        blockedTextField.setText(fileController.getPath());
        blockedTextField.setEditable(false);
        browsePanel.add(blockedTextField, BorderLayout.WEST);
        final JButton browse = new JButton("Browse...");
        browsePanel.add(browse, BorderLayout.EAST);

        browse.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                final JFileChooser fileChooser = new JFileChooser();
                int result = fileChooser.showSaveDialog(blockedTextField);
                if (result == JFileChooser.APPROVE_OPTION) {
                    fileController.setFile(fileChooser.getSelectedFile());
                    blockedTextField.setText(fileController.getPath());
                } else if (result == JFileChooser.CANCEL_OPTION) {
                } else {
                    JOptionPane.showMessageDialog(fileChooser, "An error occurred during the choice of the file.", "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        save.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
              try {
                fileController.writeOnFile(textArea.getText());
              } catch (IOException e1) {
                e1.printStackTrace();
              } 
            }
        });
    }
    /**
     * Shows the frame with the correct resolution.
     */
    private void display() {
        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
        frame.setVisible(true);
    }
    /**
     * Main method.
     * @param args
     *          Unused
     */
    public static void main(final String[] args) {
        new SimpleGUIWithFileChooser().display();
    }


}
