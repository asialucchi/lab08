package it.unibo.oop.lab.mvcio;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 
 */
public class Controller {

    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class with:
     * 
     * 1) A method for setting a File as current file
     * 
     * 2) A method for getting the current File
     * 
     * 3) A method for getting the path (in form of String) of the current File
     * 
     * 4) A method that gets a String as input and saves its content on the current
     * file. This method may throw an IOException.
     * 
     * 5) By default, the current file is "output.txt" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that runs correctly on every platform.
     */

   private File myFile;
   /**
    * default constructor, that sets the file on \home\output.txt.
    */
   public Controller() {
       myFile = new File(System.getProperty("user.home") + System.getProperty("file.separator") + "output.txt");
   }

   /**
    * Constructor.
    * @param path
    *           path of the file
    */
   public Controller(final String path) {
       myFile = new File(path);
   }
   /**
    * Setter.
    * @param f
    *       file I want to control.
    */
   public void setFile(final File f) {
       myFile = f;
   }
   /**
    * Getter.
    * @return
    *       the file I am controlling.
    */
   public File getFile() {
       return this.myFile;
   }
   /**
    * Getter.
    * @return
    *          the file's Path.
    */
   public String getPath() {
       return myFile.getPath();
   }
   /**
    * Writes a string on file or throws IOException.
    * @param str
    *           string to write on file
    * @throws IOException
    *           thorwnd exception
    */
   public void writeOnFile(final String str) throws IOException {
       try (DataOutputStream dstream = new DataOutputStream(
               new BufferedOutputStream(new FileOutputStream(myFile.getPath())))) {
           dstream.writeUTF(str + "\n");
       }
   }


}
